"use strict";

const translator = require("ks-cheap-translator");
const NotifPopup = require("./notif-popup");

module.exports = function () {
    if (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase())) {
        (new NotifPopup({
            error: true,
            message: translator.trad(
                "WARNING: This application is not designed for mobile devices. Display and functionalities may be broken."
            ),
            close_delay: 5000,
        })).pop();
    }
}