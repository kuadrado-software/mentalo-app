"use strict";

const RootPage = require("./rootpage");
const renderer = require("object-to-html-renderer");

/**
 * Makes obj2htm accessible in window scope under the key obj2htm
 */
renderer.register("obj2htm");

/**
 * Set the component RootPage as the root of the rendering cycle
 */
obj2htm.setRenderCycleRoot(new RootPage());
obj2htm.renderCycle();
