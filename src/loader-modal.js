"use strict";

/**
 * A full window loader component animated with css
 */
class LoaderModal {
    /**
     * @param {
     *      message: {String}, The message being displayed below the loader animation,
     *      override_style: {Object}, Some css rules that can be passed to override the default ones
     * } options 
     * @returns {Object} The object representation of the html to render for this component
     */
    render(options = { message: "", override_style: {} }) {
        return {
            tag: "div",
            class: "mtlo-loader-modal",
            style_rules: options.override_style,
            contents: [
                {
                    tag: "div",
                    class: "mtlo-loader-decor-wrapper",
                    contents: [
                        {
                            tag: "div",
                            class: "stirrer",
                            contents: [
                                {
                                    tag: "div", class: "stirrer-head",
                                },
                                {
                                    tag: "div", class: "stirrer-body",
                                },
                                {
                                    tag: "div", class: "stirrer-bottom",
                                }
                            ]
                        },
                        {
                            tag: "div",
                            class: "mtlo-loader-glass",
                            contents: [
                                {
                                    tag: "div",
                                    class: "mtlo-loader-liquid"
                                },
                            ]
                        },
                        {
                            tag: "div",
                            class: "ice-cube cube1"
                        },
                        {
                            tag: "div",
                            class: "ice-cube cube2"
                        },
                    ]
                },
                !!options.message && {
                    tag: "div", contents: options.message, class: "mtlo-loader-message"
                }
            ]
        }
    }
}

module.exports = LoaderModal;