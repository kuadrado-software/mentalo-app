"use strict";

const translator = require("ks-cheap-translator");
const { supported_locales, images_url } = require("../constants");
const icon_burger = require("../jsvg/icon_burger");
const t = translator.trad.bind(translator);

const FLAG_ICONS = {
    fr: `${images_url}flags/fr.svg`,
    en: `${images_url}flags/en.svg`,
    es: `${images_url}flags/es.svg`,
};

const LANGUAGES_TEXT = {
    fr: "Français",
    en: "English",
    es: "Español"
};

/**
 * A sidebar component holding useful settings and informations
 */
class MenuPanel {
    constructor(params) {
        this.params = params;
        this.state = {
            shown: false,
        };
    }

    /**
     * Handle the visibility state of the panel
     * @param {String} mode Can be toggle, hide or show
     */
    toggle_panel(mode) {
        const panel = document.getElementsByClassName("mtlo-app-menu-panel-component")[0];
        if (!panel) return;

        switch (mode) {
            case "toggle":
                panel.classList.toggle("shown");
                break;
            case "hide":
                panel.classList.remove("shown");
                break;
            case "show":
                panel.classList.add("shown");
                break;
        }

        this.state.shown = panel.classList.contains("shown");

        if (this.state.shown) {
            /**
             * Event listener to close the panel on click outside
             * @param {Event} e 
             */
            this.click_outside_listener = e => {
                const root_el = document.getElementsByClassName("mtlo-app-menu-panel-component")[0];

                const is_inside = function () {
                    if (root_el === e.target) return true;

                    for (const node of root_el.getElementsByTagName("*")) {
                        if (node === e.target) return true;
                    }

                    return false;
                }

                if (!is_inside()) {
                    this.toggle_panel("hide");
                    this.clear();
                }
            }

            window.addEventListener("click", this.click_outside_listener);

        }
        document.body.style.overflow = this.state.shown ? "hidden" : "visible";
    }

    /**
     * Clear the attached event listeners
     */
    clear() {
        if (this.click_outside_listener) {
            window.removeEventListener("click", this.click_outside_listener);
        }
    }

    /**
     * Refresh the rendering cycle from this node
     */
    refresh() {
        obj2htm.subRender(this.render(), document.getElementsByClassName("mtlo-app-menu-panel-component")[0], { mode: "replace" })
    }

    /**
     * @returns {Object} Object reprensentation of the html element fot this component
     */
    render() {
        const { on_changed_language } = this.params;
        return {
            tag: "div",
            class: "mtlo-app-menu-panel-component" + (this.state.shown ? " shown" : ""),
            contents: [
                {
                    tag: "div",
                    class: "cog-icon",
                    contents: [
                        { tag: "span", contents: t("Menu"), class: "panel-title" },
                        { ...icon_burger }
                    ],
                    onclick: this.toggle_panel.bind(this, "toggle")
                },
                {
                    tag: "aside",
                    class: "mtlo-app-menu-panel",
                    contents: [
                        {
                            tag: "div",
                            class: "language-block pref-block",
                            contents: [
                                {
                                    tag: "label", class: "setting-label", contents: t("Application language"),
                                },
                                {
                                    tag: "div",
                                    class: "flags",
                                    contents: supported_locales.map(locale => {
                                        return {
                                            tag: "img",
                                            class: translator.locale === locale ? "active" : "",
                                            src: FLAG_ICONS[locale],
                                            tooltip: LANGUAGES_TEXT[locale],
                                            /**
                                             * Handle language change when a flag gets clicked
                                             * Calls the on_changed_language callback which handles the refreshing of what needs to be refeshed.
                                             * @param {Event} e 
                                             */
                                            onclick: async e => {
                                                e.stopImmediatePropagation();
                                                if (locale !== translator.locale) {
                                                    await translator.update_translations(locale);
                                                    on_changed_language();
                                                }
                                            }
                                        }
                                    }),
                                },
                            ]
                        },
                        {
                            tag: "div",
                            class: "pref-block beta-block",
                            contents: [
                                {
                                    tag: "label", class: "setting-label", contents: t("Beta in progress"),
                                },
                                {
                                    tag: "p",
                                    contents: t("beta-in-progress.paragraph")
                                },
                            ]
                        },
                        {
                            tag: "div",
                            class: "pref-block dev-block",
                            contents: [
                                {
                                    tag: "label", class: "setting-label", contents: t("Development"),
                                },
                                {
                                    tag: "div",
                                    contents: [
                                        {
                                            tag: "p",
                                            contents: t("mentalo-dev.paragraph"),
                                        },
                                        {
                                            tag: "ul",
                                            contents: [
                                                { label: "App", link: "https://gitlab.com/kuadrado-software/mentalo-app" },
                                                { label: "Rest API", link: "https://gitlab.com/kuadrado-software/mentalo_api" },
                                                { label: "Game engine", link: "https://gitlab.com/kuadrado-software/mentalo-engine" },
                                                { label: "Drawing applet", link: "https://gitlab.com/kuadrado-software/mentalo-drawing-tool" },
                                            ].map(li => {
                                                return {
                                                    tag: "li",
                                                    contents: [
                                                        { tag: "a", contents: li.label, href: li.link, target: "_blank" }
                                                    ]
                                                }
                                            })
                                        }
                                    ]
                                },
                            ]
                        }
                    ],
                }
            ]
        }
    }
}

module.exports = MenuPanel;