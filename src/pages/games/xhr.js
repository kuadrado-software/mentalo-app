/**
 * All the async fetch calls to the api should be defined in here
 */
"use strict";

const Project = require("../../screens/app/model/project");

const ENV = "api_server";

const urls = {
    local: {
        game_list: "/mockup-data/games-list.json",
        game_search: "/mockup-data/games-list.json?",
        game: "/mockup-data/game.json",
        game_overview: "/mockup-data/game_overview.json",
        license: "/publishing/license-attribution-link",
    },
    api_server: {
        game_list: "/game/list",
        game_search: "/game/search?",
        game: "/game/{game_id}",
        game_overview: "/game/overview/{game_id}",
        publish: "/game/{mode}/{locale}",
        resend_email: "/game/send-confirmation-email",
        license: "/publishing/license-attribution-link",
        request_deletion: "/game/request-delete/{locale}"
    }
};

/**
 * A local helper to format the xhr body for game publishing.
 * Makes sure the input game data is not corrupted by loading it in a Project instance and reexporting it.
 * @param {Object} post_data 
 * @returns {Object}
 */
function __get_formatted_game_data_post(post_data) {
    // Reformat the game data by loading it in a Project instance 
    // in case the file were corrupted and to remove useless metadata.
    // Metadata are  partially returned by the server in the game objects
    // in order have access to the created_on field. But send partial metadata
    // object to the API would trigger a deserialization error.

    const game_project = new Project();
    game_project.load_data(post_data.game);
    const formatted_game_data = game_project.as_litteral();
    formatted_game_data.publishing_overview = { ...post_data.game.publishing_overview };

    return {
        game: formatted_game_data,
        user_email: post_data.user_email,
    };
}

/**
 * A generic local helper to call fetch() with given uri and options and return json for success and String for Error
 * @param {String} url 
 * @param {Object} options 
 * @returns {Promise<JSON, String>}
 */
function __fetch_json_or_text_error(url, options) {
    return new Promise((resolve, reject) => {
        fetch(url, options)
            .then(async res => {
                if (res.status >= 200 && res.status <= 300) {
                    resolve(await res.json())
                } else {
                    reject(await res.text())
                }
            })
            .catch(err => reject(err))
    })
}

/**
 * A generic local helper to call fetch() with given uri and options and return a text response
 * @param {String} url
 * @param {Object} options
 * @returns {Promise<String, String>}
 */
function __fetch_text_response(url, options) {
    return new Promise((resolve, reject) => {
        fetch(url, options)
            .then(async res => {
                if (res.status >= 200 && res.status <= 300) {
                    resolve(await res.text())
                } else {
                    reject(await res.text())
                }
            })
            .catch(err => reject(err))
    })
}

/**
 * Fetches a list of game overviews
 * @param {Integer} page 
 * @param {String} sort_mode 
 * @returns {Promise<JSON,String>}
 */
function fetch_game_list(page, sort_mode) {
    const url = urls[ENV].game_list;
    return __fetch_json_or_text_error(`${url}?page=${page}&sort_by=${sort_mode}`);
}

/**
 * Fetches a full game resource
 * @param {String} game_id
 * @returns {Promise}
 */
function fetch_game(game_id) {
    const url = urls[ENV].game.replace("{game_id}", game_id);
    return __fetch_json_or_text_error(url);
}

/**
 * Fetches the overview of one game
 * @param {String} game_id 
 * @returns {Promise}
 */
function fetch_one_game_overview(game_id) {
    const url = urls[ENV].game_overview.replace("{game_id}", game_id);
    return __fetch_json_or_text_error(url);
}

/**
 * Fetches a list of game overviews from a search query by game name
 * @param {URLSearchParams} query_string 
 * @returns {Promise}
 */
function fetch_game_search(query_string) {
    const url = urls[ENV].game_search;
    return __fetch_json_or_text_error(url + query_string);
}

/**
 * Post game data to publish or update a game
 * @param {Objet} data The publication data
 * @param {String} mode This publication mode, either "publish" or "update"
 * @param {String} locale A 2 character language code
 * @returns {Promise}
 */
function fetch_post_game_publication(data, mode, locale) {
    if (ENV === "local") {
        return new Promise((resolve, _reject) => {
            setTimeout(() => resolve({ inserted_id: { $oid: "insertedidtestlocal" } }), 1000);
            // setTimeout(() => _reject("Some error message, machin machin"), 1000);
        });
    } else {
        const url = urls[ENV].publish
            .replace("{mode}", mode)
            .replace("{locale}", locale);
        const __fetch = mode === "update" ? __fetch_text_response : __fetch_json_or_text_error;
        return __fetch(url, {
            method: mode === "update" ? "PUT" : "POST",
            body: JSON.stringify(__get_formatted_game_data_post(data)),
            headers: {
                "Content-Type": "application/json"
            },
        })
    }
}

/**
 * Sends a POST xhr to request the deletion of a game
 * @param {String} game_id The hexadecimal String of the game id
 * @param {String} email The email of the author
 * @param {String} locale A language code
 * @returns {Promise}
 */
function fetch_post_game_deletion_request(game_id, email, locale) {
    if (ENV === "local") {
        return new Promise((resolve, _reject) => {
            setTimeout(() => resolve(game_id + " " + email), 1000);
            // setTimeout(() => _reject("Some error message, machin machin"), 1000);
        });
    } else {
        const url = urls[ENV].request_deletion
            .replace("{locale}", locale);
        const form_data = new FormData();
        form_data.set("email", email);
        form_data.set("game_id", game_id);
        return __fetch_text_response(url, {
            method: "POST",
            body: new URLSearchParams(form_data),
        })
    }
}

/**
 * Sends a request to send the confirmation email again
 * @param {Object} game_publishing_data The publication data bundle related to the confirmation email that must be resent
 * @param {String} mode "publish" or "update"
 * @param {String} locale Language code
 * @returns {Promise}
 */
function resend_email(game_publishing_data, mode, locale) {
    // get mode publish or update confirmation in data
    if (ENV === "local") {
        return new Promise((resolve, _reject) => {
            setTimeout(() => resolve("ok"), 1000);
            // setTimeout(() => _reject("err"), 1000);
        });
    } else {
        const game_id = game_publishing_data.game._id.$oid;

        const form_data = new FormData();
        form_data.set("email", game_publishing_data.user_email);
        form_data.set("mode", mode);
        form_data.set("locale", locale);
        form_data.set("game_id", game_id);

        const url = urls[ENV].resend_email;
        return __fetch_text_response(url, {
            method: "POST",
            body: new URLSearchParams(form_data)
        })
    }
}

/**
 * Fetches the string uri of the licence used for the published games
 * @returns {Promise}
 */
function fetch_publishing_license_link() {
    if (ENV === "local") {
        return new Promise((resolve, _reject) => {
            setTimeout(() => resolve("https://creativecommons.org/licenses/by/4.0/"), 50);
        });
    } else {
        const url = urls[ENV].license;
        return __fetch_text_response(url)
    }

}

module.exports = {
    fetch_game_list,
    fetch_game,
    fetch_game_search,
    fetch_post_game_publication,
    resend_email,
    fetch_publishing_license_link,
    fetch_post_game_deletion_request,
    fetch_one_game_overview
}