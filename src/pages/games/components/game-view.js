"use strict";

const GameListItem = require("./game-list-item");

class GameView {
    constructor(params) {
        this.params = params;
    }

    create_modal() {
        this.modal_element = document.createElement("div");

        const style = {
            position: "fixed",
            inset: 0,
            backgroundColor: "#0009",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
        };

        Object.entries(style).forEach(k_v => {
            const [key, value] = k_v;
            this.modal_element.style[key] = value;
        });

        document.getElementById("games-page")
            .appendChild(this.modal_element);

        document.body.style.overflow = "hidden";

        this.attach_modal_events_listeners();
    }

    attach_modal_events_listeners() {
        this.click_outside_listener = e => {
            if (e.target === this.modal_element) {
                this.close();
            }
        };

        this.press_escape_listener = e => {
            if (e.key.toLowerCase() === "escape") {
                this.close();
            }
        };

        window.addEventListener("click", this.click_outside_listener);
        window.addEventListener("keydown", this.press_escape_listener);
    }

    clear_listeners() {
        window.removeEventListener("click", this.click_outside_listener);
    }

    show() {
        this.create_modal();
        obj2htm.subRender(this.render(), this.modal_element, { mode: "override" });
    }

    close() {
        this.modal_element.remove();
        this.clear_listeners();
        document.body.style.overflow = "visible";
        this.params.on_close && this.params.on_close();
    }

    render() {
        const { game } = this.params;
        return {
            tag: "div",
            id: "game-view",
            contents: [
                new GameListItem({ game, modal_view: true }).render(),
            ]
        }
    }
}

module.exports = GameView;