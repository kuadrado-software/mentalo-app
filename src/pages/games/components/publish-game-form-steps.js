"use strict";
const translator = require("ks-cheap-translator");
const { custom_project_mime } = require("../../../constants");
const icon_check = require("../../../jsvg/icon_check");
const icon_delete = require("../../../jsvg/icon_delete");
const icon_export = require("../../../jsvg/icon_export");
const icon_import = require("../../../jsvg/icon_import");
const NotifPopup = require("../../../notif-popup");
const { load_project_data } = require("../../../project-loader");
const ResourcesIndex = require("../../../screens/app/model/resources_index");
const { file_has_ext } = require("../../../utils");
const { fetch_post_game_publication, resend_email, fetch_game_search, fetch_post_game_deletion_request } = require("../xhr");
const t = translator.trad.bind(translator);

/**
 * A helper to check if an email input is a valid email address
 * @param {String} email 
 * @returns {Boolean}
 */
function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function formatBytes(bytes) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;

    const sizes = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return {
        n: parseInt((bytes / Math.pow(k, i))),
        unit: sizes[i]
    }
}

const MAX_PAYLOAD_SIZE = 1 << 25

/**
 * The component holding the differents form steps displayed as the content of the PublishGameDialog component
 */
class PublishGameFormSteps {
    constructor(params) {
        this.params = params;
        this.id = "publish-game-form-steps";

        this.state = {
            game_publishing_data: {
                user_email: "",
            },
            step_active: {
                publish: 0,
                update: 0,
                request_deletion: 0,
            },
            step_specific_listeners: [],
        };

        /**
         * An index of the successive steps to follow to achieve a game publication, 
         * update or delete regarding which mode is selected
         */
        this.steps = {
            publish: [
                {
                    label: "Load your game file",
                    short_label: "File",
                    render: this.render_step_load_file.bind(this, 0)
                },
                {
                    label: "Publication information",
                    short_label: "Infos",
                    render: this.render_step_form_infos.bind(this, 1)
                },
                {
                    label: "Choose a banner image",
                    short_label: "Banner",
                    render: this.render_step_banner.bind(this, 2)
                },
                {
                    label: "Send the publication",
                    short_label: "Publish",
                    render: this.render_step_send_publish.bind(this)
                }
            ],
            update: [
                {
                    label: "Find your game",
                    short_label: "Find",
                    render: this.render_step_find_game.bind(this)
                },
                {
                    label: "Load the new version",
                    short_label: "File",
                    render: this.render_step_load_file.bind(this, 1)
                },
                {
                    label: "Publication information",
                    short_label: "Infos",
                    render: this.render_step_form_infos.bind(this, 2)
                },
                {
                    label: "Choose a banner image",
                    short_label: "Banner",
                    render: this.render_step_banner.bind(this, 3)
                },
                {
                    label: "Send the new version",
                    short_label: "Publish",
                    render: this.render_step_send_update.bind(this)
                }
            ],
            request_deletion: [
                {
                    label: "Find your game",
                    short_label: "Find",
                    render: this.render_step_find_game.bind(this)
                },
                {
                    label: "Send you deletion demand",
                    short: "Send",
                    render: this.render_step_send_deletion_request.bind(this)
                }
            ]
        };
    }

    /**
     * Removes the event listeners that were attached specifically for the currently selected step.
     */
    clean_step_specific_listeners() {
        this.state.step_specific_listeners.forEach(item => {
            window.removeEventListener(item.type, item.cb);
        });
        this.state.step_specific_listeners = [];
    }

    /**
     * @returns {Array<Object>} The set of form steps corresponding to the currently selected mode
     */
    get_steps() {
        return this.steps[this.params.get_mode()];
    }

    /**
     * @returns {Object} the currently selected step
     */
    get_step_active() {
        return this.state.step_active[this.params.get_mode()]
    }

    /**
     * Handles changes from file input for the banner image step
     * @param {Integer} complete_step_on_success The index of the step that will be completed after this action
     * @param {Event} e 
     */
    handle_change_banner_image(complete_step_on_success, e) {
        const files = e.target.files;
        if (files && files[0]) {
            const file = files[0];
            if (file.size > 300 * 1000) {
                (new NotifPopup({
                    error: true,
                    message: t("This image is too heavy. Please select an image with size below 300KB")
                })).pop();
            } else {
                this.handle_load_banner_image(files[0], complete_step_on_success);
            }
        }
    }

    /**
     * Reads the data from an image file, parses it as base64 data, 
     * sets it as the game_data new banner image and refreshes the view
     * @param {File} file 
     * @param {Integer} complete_step_on_success The index of the step that will be completed after this action
     */
    handle_load_banner_image(file, complete_step_on_success) {
        const fr = new FileReader();
        fr.onload = () => {
            const img_data = fr.result;
            this.state.game_publishing_data.game.publishing_overview.banner_image = img_data;
            this.get_steps()[complete_step_on_success].complete = true;
            this.refresh();
        };
        fr.readAsDataURL(file);
    }

    /**
     * Handle change from file input for the step "load game file"
     * @param {Event} e 
     * @param {Integer} step_index the index of the step being rendered
     */
    handle_change_game_file(step_index, e) {
        const files = e.target.files;
        if (files && files[0]) {
            if (!file_has_ext(files[0], custom_project_mime)) {
                (new NotifPopup({
                    error: true,
                    message: t(
                        "Please select a .*** file",
                        { ext: custom_project_mime })
                })).pop()
            } else {
                this.handle_load_game(files[0], step_index);
            }
        }
    }

    /**
     * Optimizes the data of a loaded game by removing unused resources.
     * @param {Object} data Litteral game data loaded from file
     * @returns {Object} The updated data
     */
    get_optimized_data(data) {
        const resources_index = new ResourcesIndex();

        resources_index.load_data(data.resources_index);
        resources_index.delete_unused_resources("images", data.scenes, "raw");
        resources_index.delete_unused_resources("sounds", data.scenes, "raw");

        data.resources_index = resources_index.as_litteral();

        return data;
    }

    /**
     * Makes a comparison between the size of the game data and the maximum allowed payload size.
     * @param {Object} data  The game data loaded from file
     * @returns {Boolean} True if the data size is inferior to max size.
     */
    validate_data_size(data) {
        const blob = new Blob([JSON.stringify(data)], { type: "application/json" })
        return blob.size <= MAX_PAYLOAD_SIZE
    }

    /**
     * Reads the data from a .mtl (Mentalo game project) file and assigns it to the 
     * game_publising_data state or the game_to_update state regarding the mode state is publish or update.
     * @param {File} file 
     * @param {Integer} complete_step_on_success The index of the step that will be completed after this action
     */
    handle_load_game(file, complete_step_on_success) {
        load_project_data(file)
            .then(game => {

                game = this.get_optimized_data(game);

                if (!this.validate_data_size(game)) {
                    const format_max_bytes = formatBytes(MAX_PAYLOAD_SIZE)
                    const message = t(
                        `Your file is too large. The maximum size to publish a game is XXX.`,
                        { size: `${format_max_bytes.n} ${t(format_max_bytes.unit)}` }
                    );
                    (new NotifPopup({ error: true, message })).pop();
                    return
                }

                if (this.state.game_to_update && this.params.get_mode() === "update") {
                    if (this.state.game_to_update.name !== game.name && !window.confirm(t("warn.new-version-same-title"))) {
                        this.params.on_close();
                        return;
                    }

                    game.name = this.state.game_to_update.name;

                    Object.assign(this.state.game_to_update, game);
                    this.state.game_publishing_data.game = this.state.game_to_update;
                } else {
                    game.publishing_overview = {};
                    this.state.game_publishing_data.game = game;
                }

                this.get_steps()[complete_step_on_success].complete = true;
                this.refresh();
            })
            .catch(err => (new NotifPopup({ error: true, message: err })).pop())
    }

    /**
     * Handles changes from the form displayed in the "Information" step
     * @param {Integer} complete_step_on_success The index of the step that will be completed after this action
     * @param {Event} e
     */
    handle_form_info_change(complete_step_on_success, e) {
        this.state.game_publishing_data
            .game.publishing_overview[e.target.name] = e.target.value;
        this.get_steps()[complete_step_on_success].complete = document.getElementById("game-publish-infos-form").checkValidity();
        this.refresh_steps_nav();
    }

    /**
     * Handles the click on the previous or next buttons.
     * @param {Integer} index The index of the step we want to mode to.
     */
    handle_nav_step(index) {
        if (index >= 0 && index <= this.get_steps().length - 1) {
            this.clean_step_specific_listeners();
            this.state.step_active[this.params.get_mode()] = index;
            this.refresh();
        }
    }

    /**
     * Handles the submitting of the search query in the step "Find game"
     * @param {Event} e
     */
    handle_submit_search_game(e) {
        e.preventDefault();
        const form_data = new FormData(e.target);
        const clean_query = form_data.get("query").trim();
        if (clean_query.length < 3) {
            (new NotifPopup({
                error: true,
                message: t("Please type at least 3 characters to make a search query.")
            })).pop();
        } else {
            form_data.set("query", clean_query);
            const query = new URLSearchParams(form_data);
            const step = this.steps[this.params.get_mode()][0];
            step.processing = true;
            step.query = form_data.get("query");
            this.refresh_update_game_find_result();
            fetch_game_search(query)
                .then(res => {
                    step.search_results = res;
                })
                .catch(err => {
                    step.error = err;
                })
                .finally(() => {
                    step.processing = false;
                    this.refresh_update_game_find_result();
                })
        }

    }

    /**
     * Handle changes from checkboxes in the search game results to select the game to update or delete.
     * @param {Event} e 
     */
    handle_change_game_to_update(step, game, e) {
        if (this.params.get_mode() === "update") {
            this.state.game_to_update = e.target.checked ? game : undefined;
        } else if (this.params.get_mode() === "request_deletion") {
            this.state.game_publishing_data.game = e.target.checked ? game : undefined;
        }
        step.complete = e.target.checked;
        this.refresh();
    }

    /**
     * Handles click on the resend confirmation email button
     */
    handle_resend_confirmation_email() {
        obj2htm.subRender(
            { tag: "span", id: "resend-email-status", class: "spin-loader" },
            document.getElementById("resend-email-status"),
            { mode: "replace" }
        );

        resend_email(
            this.state.game_publishing_data,
            this.params.get_mode(),
            translator.locale
        )
            .then(() => {
                obj2htm.subRender(
                    { tag: "span", id: "resend-email-status", contents: "<green>Ok</green>" },
                    document.getElementById("resend-email-status"),
                    { mode: "replace" }
                );
            })
            .catch(() => {
                obj2htm.subRender(
                    { tag: "span", id: "resend-email-status", contents: `<red>${t("Error")}</red>` },
                    document.getElementById("resend-email-status"),
                    { mode: "replace" }
                );
            })
    }

    handle_submit_game_publication() {
        const steps = this.get_steps();
        const final_step = steps[steps.length - 1];

        final_step.processing = true;

        this.refresh_final_step_status();

        fetch_post_game_publication(
            this.state.game_publishing_data,
            this.params.get_mode(),
            translator.locale
        )
            .then(success => {
                if (typeof success === "object" && success.inserted_id) {
                    this.state.game_publishing_data.game._id = { $oid: success.inserted_id };
                }
                final_step.success = success;
                final_step.error = undefined;
            })
            .catch(err => {
                final_step.success = undefined;
                final_step.error = err;
                (new NotifPopup({
                    error: true,
                    message: err
                })).pop();
            })
            .finally(() => {
                final_step.processing = false;
                final_step.finished = true;
                this.refresh();
            });
    }

    handle_submit_deletion_request() {
        const steps = this.get_steps();
        const final_step = steps[steps.length - 1];

        final_step.processing = true;
        this.refresh_final_step_status();

        fetch_post_game_deletion_request(
            this.state.game_publishing_data.game._id.$oid,
            this.state.game_publishing_data.user_email,
            translator.locale
        )
            .then(success => {
                if (typeof success === "object" && success.inserted_id) {
                    this.state.game_publishing_data.game._id = { $oid: success.inserted_id };
                }
                final_step.success = success;
                final_step.error = undefined;
            })
            .catch(err => {
                final_step.success = undefined;
                final_step.error = err;
                (new NotifPopup({
                    error: true,
                    message: err
                })).pop();
            })
            .finally(() => {
                final_step.processing = false;
                final_step.finished = true;
                this.refresh();
            });
    }

    /**
     * @returns {Object} The object representation of the html to render for the "load game file" step
     */
    render_step_load_file(step_index) {
        const { game } = this.state.game_publishing_data;

        return {
            tag: "div",
            class: "step-contents",
            style_rules: {
                display: "grid",
                gridTemplateColumns: "auto auto",
                gap: "40px"
            },
            contents: [
                {
                    tag: "p", class: "step-info", style_rules: { flex: 1 },
                    contents: t("Browse your computer and select the .mtl file you exported from the project editor.")
                },
                {
                    tag: "input", type: "file",
                    id: "publish-game-file-input",
                    style_rules: { display: "none" },
                    onchange: this.handle_change_game_file.bind(this, step_index)
                },
                {
                    tag: "label", htmlFor: "publish-game-file-input",
                    class: "load-game-file-btn", contents: [{ ...icon_import }],
                },
                game && {
                    tag: "div", class: "loaded-game-name",
                    style_rules: { gridColumn: "1 / span 2" },
                    contents: `${t("Loaded game")} : <b>${game.name}</b>`
                }
            ]
        }
    }

    /**
     * @returns {Object} Object representation of the html to render for the "Find game" step
     * This step can be used in delete or update mode.
     */
    render_step_find_game() {
        return {
            tag: "div",
            class: "step-contents",
            contents: [
                {
                    tag: "p", class: "step-info",
                    contents: this.params.get_mode() === "request_deletion" ?
                        t("Find the the game you want to delete")
                        : t("Find the previous version of the game you want to update.")
                },
                {
                    tag: "form",
                    class: "search-bar",
                    onsubmit: this.handle_submit_search_game.bind(this),
                    contents: [
                        {
                            tag: "input", type: "search", name: "query",
                            placeholder: t("Type the name of the game"),
                            value: this.steps.update[0].query || "",
                        },
                        { tag: "input", type: "submit", value: t("Find") }
                    ]
                },
                this.render_find_game_results()
            ]
        }
    }

    /**
     * @returns {Object} the object representation of html to render for the list of search results in the step "Find game"
     */
    render_find_game_results() {
        const step = this.steps[this.params.get_mode()][0];
        const { search_results } = step;

        if (!this.state.step_specific_listeners.find(l => l.name === "list-scroll-listener")) {
            const len = this.state.step_specific_listeners.push({
                type: obj2htm.event_name,
                name: "list-scroll-listener",
                cb: e => {
                    const list = e.detail.outputNode.querySelector("#update-game-find-results ul");
                    list && list.scrollTo(0, this.steps.update[0].scroll_y || 0);
                }
            });
            window.addEventListener(
                obj2htm.event_name,
                this.state.step_specific_listeners[len - 1].cb
            );
        }

        return {
            tag: "div",
            id: "update-game-find-results",
            contents: step.processing ? [{
                tag: "div", class: "spin-loader"
            }] : step.search_results ? [{
                tag: "div",
                contents: [
                    {
                        tag: "div",
                        style_rules: {
                            display: "flex", justifyContent: "space-between",
                            alignItems: "center", padding: "5px 0"
                        },
                        contents: [

                            {
                                tag: "span", contents: this.params.get_mode() === "request_deletion" ?
                                    t("Select the game to delete")
                                    : t("Select the game to update"),
                            },
                            { tag: "span", contents: `${t("Results")}: <green>${search_results.length}</green>` },
                        ]
                    },
                    {
                        tag: "ul",
                        onscroll: e => { step.scroll_y = e.target.scrollTop },
                        contents: search_results.map(game => {
                            return {
                                tag: "li",
                                class: ((
                                    this.params.get_mode() === "update" ?
                                        this.state.game_to_update :
                                        this.state.game_publishing_data.game
                                ) === game ? "selected" : ""),
                                contents: [
                                    {
                                        tag: "div", class: "img-container", contents: [
                                            { tag: "img", src: game.publishing_overview.banner_image },
                                        ]
                                    },
                                    { tag: "strong", contents: game.name },
                                    { tag: "span", contents: `${t("By")} ${game.publishing_overview.author_name}` },
                                    {
                                        tag: "div", class: "checkbox-container", contents: [
                                            {
                                                tag: "input", type: "checkbox", checked: (
                                                    this.params.get_mode() === "update" ?
                                                        this.state.game_to_update :
                                                        this.state.game_publishing_data.game
                                                ) === game,
                                                tooltip: t("Select this game"),
                                                onchange: this.handle_change_game_to_update.bind(this, step, game)
                                            }
                                        ]
                                    }
                                ]
                            }
                        })
                    }
                ]
            }] : step.error ? [
                {
                    tag: "strong",
                    contents: `<red>${t("Server error")}</red>: `
                },
                { tag: "span", contents: step.error }
            ] : undefined
        }
    }

    /**
     * Refreshes the rendering of the search game results i the "Find game" step
     */
    refresh_update_game_find_result() {
        obj2htm.subRender(
            this.render_find_game_results(),
            document.getElementById("update-game-find-results"),
            { mode: "replace" }
        )
    }

    /**
     * @param {Integer} step_index The index of the step the form is being rendered in.
     * @returns {Object} The object representation of the html to render for the Informations step.
     */
    render_step_form_infos(step_index) {
        const { publishing_overview } = this.state.game_publishing_data.game;
        const step = this.get_steps()[step_index];

        if (publishing_overview.author_name && publishing_overview.description && !step.complete) {
            step.complete = true;
        }

        return {
            tag: "form",
            id: "game-publish-infos-form",
            class: "step-contents",
            style_rules: {
                display: "grid",
                gridTemplateColumns: "1fr 1fr",
                gap: "10px"
            },
            contents: [
                {
                    tag: "p", class: "step-info", style_rules: { gridColumn: "1 /span 2" },
                    contents: t("Fill some public information about you and your game.")
                },
                {
                    tag: "input", type: "text", name: "author_name", placeholder: t("Your author name"),
                    value: publishing_overview.author_name || "",
                    required: true,
                    oninput: this.handle_form_info_change.bind(this, step_index),
                },
                {
                    tag: "input", type: "url", name: "author_website", placeholder: t("A web site (optional)"),
                    value: publishing_overview.author_website || "",
                    oninput: this.handle_form_info_change.bind(this, step_index),
                    onblur: e => e.target.reportValidity()
                },
                {
                    tag: "textarea",
                    style_rules: { resize: "none", height: "160px", gridColumn: "1 / span 2" },
                    name: "description", placeholder: t("Description of your game"), required: true,
                    value: publishing_overview.description || "",
                    oninput: this.handle_form_info_change.bind(this, step_index),
                },
                {
                    tag: "p",
                    class: "note-give-credit",
                    contents: t("If you created this game from another one, please use the description to credit the author of the game on which you based yours.")
                }
            ]
        }
    }

    /**
     * @param {Integer} step_index The index of the step the form is being rendered in.
     * @returns {Object} The object representation of the html to render for the Banner image step.
     */
    render_step_banner(step_index) {
        const { publishing_overview } = this.state.game_publishing_data.game;
        const step = this.get_steps()[step_index];

        if (publishing_overview.banner_image && !step.complete) {
            step.complete = true;
            this.refresh();
        }

        return {
            tag: "div",
            class: "step-contents",
            style_rules: {
                display: "grid",
                gridTemplateColumns: "auto auto"
            },
            contents: [
                {
                    tag: "p", class: "step-info", style_rules: { flex: 1 },
                    contents: t("Choose a banner image for the public overview of your game.")
                },
                {
                    tag: "input", type: "file", accept: ".png,.bmp,.jpg,.jpeg,.gif",
                    id: "publish-game-banner-file-input",
                    style_rules: { display: "none" },
                    onchange: this.handle_change_banner_image.bind(this, step_index)
                },
                {
                    tag: "label", htmlFor: "publish-game-banner-file-input",
                    class: "load-game-file-btn", contents: [{ ...icon_import }],
                },
                publishing_overview.banner_image && {
                    tag: "div",
                    class: "step-banner-overview",
                    contents: [{
                        tag: "img",
                        src: publishing_overview.banner_image
                    }]
                },
            ]
        }
    }

    /**
     * @returns {Object} the object representation of the html to render for the final "Send publication" step.
     */
    render_step_send_publish() {
        return {
            tag: "div",
            class: "step-contents final-step",
            contents: [
                {
                    tag: "p", class: "step-info",
                    contents: t("Everything is set, it's time to send the package!")
                },
                {
                    tag: "p", class: "step-final-explain",
                    contents: t("info.publish-legal")
                },
                {
                    tag: "div",
                    style_rules: {
                        display: "flex",
                        gap: "10px",
                        alignItems: "center",
                        flex: 1,
                    },
                    contents: [
                        {
                            tag: "input", type: "email",
                            style_rules: { width: "200px" },
                            value: this.state.game_publishing_data.user_email,
                            placeholder: t("Your email address"),
                            required: true,
                            oninput: e => {
                                const steps = this.get_steps();
                                this.state.game_publishing_data.user_email = e.target.value;
                                steps[steps.length - 1].complete = validateEmail(e.target.value);
                                this.refresh_final_send_button()
                            }
                        },
                        this.render_final_send_button(),
                        this.render_final_step_status(),
                    ]
                }
            ]
        }
    }

    /**
     * @returns {Object} the object representation of the html to render for the final "Send update" step.
     */
    render_step_send_update() {
        const { game } = this.state.game_publishing_data;
        return {
            tag: "div",
            class: "step-contents final-step",
            contents: [
                {
                    tag: "p", class: "step-info",
                    contents: t("Your new version of *** is ready to be sent!", { game_name: game.name })
                },
                {
                    tag: "p", class: "step-final-explain",
                    contents: t("info.update-legal")
                },
                {
                    tag: "div",
                    style_rules: {
                        display: "flex",
                        gap: "10px",
                        alignItems: "center",
                        flex: 1,
                    },
                    contents: [
                        {
                            tag: "input", type: "email",
                            style_rules: { width: "200px" },
                            value: this.state.game_publishing_data.user_email,
                            placeholder: t("Your email address"),
                            required: true,
                            oninput: e => {
                                const steps = this.get_steps();
                                this.state.game_publishing_data.user_email = e.target.value;
                                steps[steps.length - 1].complete = validateEmail(e.target.value);
                                this.refresh_final_send_button()
                            }
                        },
                        this.render_final_send_button(),
                        this.render_final_step_status(),
                    ]
                }
            ]
        }
    }

    /**
     * @returns {Object} The object representation of the html to render for the final "Send deletion request" step
     */
    render_step_send_deletion_request() {
        const { game } = this.state.game_publishing_data;
        return {
            tag: "div",
            class: "step-contents final-step",
            contents: [
                {
                    tag: "p", class: "step-info",
                    contents: t("You are about to send a deletion demand for ***", { game_name: game.name })
                },
                {
                    tag: "p", class: "step-final-explain",
                    contents: t("info.deletion")
                },
                {
                    tag: "div",
                    style_rules: {
                        display: "flex",
                        gap: "10px",
                        alignItems: "center",
                        flex: 1,
                    },
                    contents: [
                        {
                            tag: "input", type: "email",
                            style_rules: { width: "200px" },
                            value: this.state.game_publishing_data.user_email,
                            placeholder: t("Your email address"),
                            required: true,
                            oninput: e => {
                                const steps = this.get_steps();
                                this.state.game_publishing_data.user_email = e.target.value;
                                steps[steps.length - 1].complete = validateEmail(e.target.value);
                                this.refresh_final_send_delete_button()
                            }
                        },
                        this.render_final_delete_button(),
                        this.render_final_step_status(),
                    ]
                }
            ]
        }
    }

    /**
     * @returns {Object} the object representation of the html to render for the request status in the final step.
     */
    render_final_step_status() {
        const steps = this.get_steps();
        const final_step = steps[steps.length - 1];
        return {
            tag: "div",
            id: "publish-game-final-step-status",
            contents: final_step.processing ?
                [
                    {
                        tag: "div",
                        style_rules: {
                            display: "flex",
                            gap: "10px",
                            fontSize: "14px",
                            alignItems: "center",
                            color: "#fff9"
                        },
                        contents: [
                            { tag: "div", class: "spin-loader" },
                            { tag: "span", style_rules: { flex: 1 }, contents: t("Please wait, this can take a while") }
                        ]
                    }

                ] : final_step.success ? [{
                    tag: "div",
                    contents: [
                        {
                            tag: "div",
                            style_rules: {
                                display: "flex",
                                alignItems: "center",
                                gap: "5px"
                            },
                            contents: [
                                { tag: "div", class: "status-icon", contents: [{ ...icon_check }] },
                                { tag: "strong", contents: `<green>${t("It's sent!")}</green>` },
                            ]
                        },
                        {
                            tag: "div",
                            class: "if-email-not-received",
                            contents: [
                                { tag: "span", contents: `${t("If you didn't receive the email")}, ` },
                                {
                                    tag: "button", contents: t("try send it again"),
                                    onclick: this.handle_resend_confirmation_email.bind(this)
                                },
                                { tag: "span", id: "resend-email-status" }
                            ]
                        }
                    ],
                }] : final_step.error ?
                    [{
                        tag: "div",
                        contents: [
                            {
                                tag: "div",
                                style_rules: {
                                    display: "flex",
                                    alignItems: "center",
                                    gap: "5px"
                                },
                                contents: [
                                    { tag: "div", class: "status-icon", contents: [{ ...icon_delete }] },
                                    { tag: "red", contents: t("Server error") }]
                            }
                        ],
                    }] : undefined,
        }
    }

    /**
     * @returns {Object} the object representation of the html to render 
     * for the Delete button in the final step of the request_deletion mode.
     */
    render_final_delete_button() {
        const steps = this.get_steps();
        const final_step = steps[steps.length - 1];
        return {
            tag: "button", contents: [{ ...icon_export }],
            tooltip: t("Send my deletion demand"),
            id: "delete-game-step-final-send-button",
            disabled: !final_step.complete,
            onclick: this.handle_submit_deletion_request.bind(this)
        }
    }

    /**
     * @returns {Object} the object representation of the html to render for the Send button in the final step.
     */
    render_final_send_button() {
        const steps = this.get_steps();
        const final_step = steps[steps.length - 1];
        return {
            tag: "button", contents: [{ ...icon_export }],
            tooltip: t("Send your game!"),
            id: "publish-game-step-final-send-button",
            disabled: !final_step.complete,
            onclick: this.handle_submit_game_publication.bind(this),
        }
    }

    /**
     * Refreshes the request status in the final step
     */
    refresh_final_step_status() {
        obj2htm.subRender(
            this.render_final_step_status(),
            document.getElementById("publish-game-final-step-status"),
            { mode: "replace" }
        );
    }

    /**
     * Refreshes the Send button in the final step
     */
    refresh_final_send_button() {
        obj2htm.subRender(
            this.render_final_send_button(),
            document.getElementById("publish-game-step-final-send-button"),
            { mode: "replace" }
        );
    }

    /**
     * Refreshes the Delete button in the final step in request_deletion mode
     */
    refresh_final_send_delete_button() {
        obj2htm.subRender(
            this.render_final_delete_button(),
            document.getElementById("delete-game-step-final-send-button"),
            { mode: "replace" }
        );
    }

    /**
     * @returns {Object} object representation of the html to render for the Previous and Next buttons
     */
    render_steps_nav() {
        const steps = this.get_steps();
        const step_active = this.get_step_active()
        const step = steps[step_active];

        return {
            tag: "div", id: "steps-nav", contents: [
                {
                    tag: "button", contents: t("Previous"),
                    onclick: this.handle_nav_step.bind(this, step_active - 1),
                    disabled: step_active === 0,
                },
                {
                    tag: "button", contents: t("Next"),
                    onclick: this.handle_nav_step.bind(this, step_active + 1),
                    disabled: !(step_active < steps.length - 1) || !step.complete
                },
                step_active === steps.length - 1 && steps[steps.length - 1].finished && {
                    tag: "button", contents: t("Close"), class: "close-btn",
                    onclick: this.params.on_close,
                }
            ]
        }
    }

    /**
     * Refreshes the rendering of the Previous and Next buttons
     */
    refresh_steps_nav() {
        obj2htm.subRender(this.render_steps_nav(), document.getElementById("steps-nav"), { mode: "replace" })
    }

    /**
     * @returns {Object} The object representation of the html to render for the steps progression line
     */
    render_steps_progress() {
        const steps = this.get_steps();
        const step_active = this.get_step_active();
        return {
            tag: "div", class: "steps-progress", id: "publish-game-steps-progress",
            contents: steps.map((stp, i) => {
                return {
                    tag: "div",
                    class: "steps-progress-step " + (
                        stp.error ?
                            "error" : stp.complete ?
                                "complete" :
                                i === step_active ?
                                    "active" : ""
                    ),
                    contents: [
                        {
                            tag: "span",
                            class: "step-progress-bullet",
                            contents: (i + 1).toString()
                        },
                        {
                            tag: "span", class: "step-progress-label",
                            contents: t(stp.short_label)
                        }
                    ]
                }
            }),
        }
    }

    /**
     * Refreshes the rendering of the steps progression line
     */
    refresh_steps_progress() {
        obj2htm.subRender(this.refresh_steps_progress(), document.getElementById("publish-game-steps-progress"), { mode: "replace" })
    }

    /**
     * Refreshes the rendering from this component root
     */
    refresh() {
        obj2htm.subRender(this.render(), document.getElementById(this.id), { mode: "replace" })
    }

    /**
     * @returns {Object} the object representation of the html to render for this component
     */
    render() {
        const steps = this.get_steps();
        const step_active = this.get_step_active();
        const step = steps[step_active];
        return {
            tag: "div", id: this.id, contents: [
                this.render_steps_progress(),
                {
                    tag: "div", class: "step-header", contents: [
                        { tag: "span", class: "step-number", contents: (step_active + 1).toString() },
                        { tag: "h3", contents: t(step.label) },
                    ]
                },
                step.render(),
                this.render_steps_nav(),
            ]
        }
    }
}

module.exports = PublishGameFormSteps;