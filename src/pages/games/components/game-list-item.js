"use strict";

const { custom_project_mime } = require("../../../constants");
const icon_pen = require("../../../jsvg/icon_pen");
const icon_export = require("../../../jsvg/icon_export");
const icon_play = require("../../../jsvg/icon_play");
const { fetch_game } = require("../xhr");
const { MentaloEngine } = require("mentalo-engine");
const translator = require("ks-cheap-translator");
const NotifPopup = require("../../../notif-popup");
const t = translator.trad.bind(translator);
const LoaderModal = require("../../../loader-modal");
const icon_link = require("../../../jsvg/icon_link");

/**
 * The component that displays a game overview item in a game list
 */
class GameListItem {
    constructor(params) {
        this.params = params;
        const game_id = this.params.game._id.$oid;
        this.id = this.params.modal_view ? `game-view-${game_id}` : `game-list-item-${game_id}`;
        this.state = { loading: false, loading_message: "" };
    }

    /**
     * Handles click on the download button.
     * Fetches the full game, builds a blob from the data and prompt the download of the file
     */
    handle_download() {
        this.show_loading_state(true, t("Download in preparation") + "...");
        const game_id = this.params.game._id.$oid;
        fetch_game(game_id)
            .then(game => {
                const blob = new Blob([JSON.stringify(game)], { type: "application/json" });
                const url = URL.createObjectURL(blob);

                const link = document.createElement("a");
                link.href = url;
                link.download = `${game.name.replace(/\s+/g, "-")}.${custom_project_mime}`;
                link.click();

                URL.revokeObjectURL(url);
                link.remove();
            })
            .catch(err => {
                (new NotifPopup({ error: true, message: err })).pop();
                console.log(err)
            })
            .finally(this.show_loading_state.bind(this, false));
    }

    /**
     * Handles click on the play button.
     * Creates a MentaloEngine instance and runs it with the game loaded in it.
     */
    handle_play_game() {
        this.show_loading_state(true, t("Game is loading") + "...");
        const game_id = this.params.game._id.$oid;
        fetch_game(game_id)
            .then(game_data => {
                const container = document.createElement("div");
                container.style.zIndex = 50;
                container.style.position = "fixed";
                container.style.inset = 0;
                container.style.display = "flex";
                container.style.justifyContent = "center";
                container.style.alignItems = "center";
                document.body.appendChild(container);
                document.body.style.overflow = "hidden";

                const engine = new MentaloEngine({
                    game_data,
                    fullscreen: true, // This may be blocked by some browser if the promise is a little slow to respond
                    frame_rate: 30,
                    use_locale: translator.locale,
                    container,
                    on_quit_game: () => {
                        container.remove();
                        document.body.style.overflow = "visible";
                    }
                });

                engine.init();
                engine.run_game();
            })
            .catch(err => {
                (new NotifPopup({ error: true, message: err })).pop();
                console.log(err)
            })
            .finally(this.show_loading_state.bind(this, false));
    }

    /**
     * Handle click on the edit game button.
     * Navigates to the root page of the app with an edit_game search param.
     * The home page is designed to automatically fetch the game and open the editor with the game data when this param is present.
     */
    handle_edit_game() {
        window.location.assign("/?edit_game=" + this.params.game._id.$oid)
    }

    /**
     * Helper to get either the created_on or last_update_on from a game in dd/mm/YYYY format.
     * @param {String} date_type 
     * @returns {String}
     */
    get_game_date_text(date_type = "created") {
        const value = date_type === "updated" ?
            this.params.game.metadata.last_update_on.$date :
            this.params.game.metadata.created_on.$date;
        const date = new Date(value);
        return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
    }

    /**
     * @returns {Object} object representation of the html to render for the game description
     */
    render_game_description() {
        const { modal_view } = this.params;
        const { description } = this.params.game.publishing_overview;
        const max_chars = 85;

        const element = {
            tag: "em",
            class: "game-list-item-description"
        };

        if (description.length > max_chars && !modal_view) {
            element.contents = `“${description.slice(0, max_chars)} [...]”`;
            element.tooltip = description.replace(/\n/g, "<br>");
        } else {
            element.contents = `“${description.replace(/\n/g, "<br>")}”`
        }

        return element;
    }

    /**
     * Toggles the loading state of this list item.
     * @param {Boolean} value 
     * @param {String} message 
     */
    show_loading_state(value, message = "") {
        this.state.loading = value;
        if (value) {
            this.state.loading_message = message;
        }
        this.refresh();
    }

    /**
     * Refreshes the rendering tree from this component
     */
    refresh() {
        obj2htm.subRender(this.render(), document.getElementById(this.id), { mode: "replace" });
    }

    /**
     * 
     * @returns {Object} object representation of the html to render for this component
     */
    render() {
        const { game, modal_view, show_game_view } = this.params;
        const { banner_image, author_name, author_website } = game.publishing_overview;
        const { version } = game.metadata;

        return {
            tag: modal_view ? "div" : "li",
            id: this.id,
            class: modal_view ? "game-view" : "game-list-item",
            contents: (this.state.loading ? [new LoaderModal().render({
                message: this.state.loading_message,
                override_style: {
                    position: "absolute",
                    backgroundColor: "#000b"
                }
            })] : []).concat([
                {
                    tag: "div",
                    class: "game-banner",
                    onclick: () => !modal_view && show_game_view(game),
                    contents: [
                        { tag: "img", src: banner_image },
                    ]
                },
                {
                    tag: "div",
                    class: "game-info",
                    contents: [
                        {
                            tag: "strong", class: "game-title",
                            contents: `<green>${game.name}</green>`,
                            onclick: () => !modal_view && show_game_view(game),
                        },
                        {
                            tag: "div",
                            class: "game-details",
                            contents: [
                                {
                                    tag: "span", class: "game-author",
                                    contents: `${t("By")} <green>${author_name}</green>`
                                },
                                !!author_website && {
                                    tag: "a",
                                    class: "game-btn",
                                    contents: [{ tag: "span", contents: "@" }],
                                    href: author_website,
                                    target: "_blank",
                                    tooltip: author_website,
                                },
                                {
                                    tag: "span", class: "game-date",
                                    contents: t(`Published the dd/mm/yyyy`, { date_text: this.get_game_date_text() })
                                },
                                version > 1 && {
                                    tag: "span",
                                    class: "game-version",
                                    contents: [{ tag: "span", contents: "v" + version }],
                                    tooltip: `${t("Version n°")}${version} <br />${t("Updated the")} ${this.get_game_date_text("updated")}`
                                },
                            ]
                        },
                        this.render_game_description()
                    ]
                },
                {
                    tag: "div",
                    class: "game-actions",
                    contents: [
                        {
                            tag: "button",
                            class: "game-btn",
                            contents: [{ ...icon_play }],
                            tooltip: t("Play"),
                            onclick: this.handle_play_game.bind(this)
                        },
                        {
                            tag: "button",
                            class: "game-btn",
                            contents: [{ ...icon_export }],
                            tooltip: t("Download"),
                            onclick: this.handle_download.bind(this)
                        },
                        {
                            tag: "button",
                            class: "game-btn",
                            contents: [{ ...icon_pen }],
                            tooltip: t("Open in the editor"),
                            onclick: this.handle_edit_game.bind(this)
                        },
                        modal_view && {
                            tag: "button",
                            class: "game-btn",
                            contents: [{ ...icon_link, style_rules: { width: "80%" } }],
                            tooltip: t("Copy game link"),
                            onclick: () => {
                                navigator.clipboard.writeText(window.location);
                                (new NotifPopup({ message: t("Link copied to clipboard") })).pop()
                            }
                        }
                    ]
                }
            ])
        }
    }
}

module.exports = GameListItem;