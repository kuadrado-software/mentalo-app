"use strict";
const { images_url, app_translations_url } = require("../../constants");
const warnNoMobile = require("../../warn-no-mobile");
const GameList = require("./components/game-list");
const TooltipsManager = require("tooltips-manager");
const PublishGameModal = require("./components/publish-game-modal");
const MenuPanel = require("../../menu-panel/menu-panel");
const LoaderModal = require("../../loader-modal");
const translator = require("ks-cheap-translator");
const NotifPopup = require("../../notif-popup");
const t = translator.trad.bind(translator);

/**
 * The component at the root of the games/ page
 */
class GamesPage {
    constructor() {
        this.tooltips_manager = new TooltipsManager({
            style: {
                backgroundColor: "#222d", maxWidth: "500px", zIndex: 10,
            },
            pop_tooltip_delay: 150,
        });

        this.menu_panel = new MenuPanel({
            on_changed_language: this.refresh.bind(this),
        });

        this.state = { loading: true };

        this.components = {
            game_list: new GameList({
                open_publish_modal: this.open_publish_modal.bind(this),
                close_publish_modal: this.close_publish_modal.bind(this)
            }),
            publish_game_modal: new PublishGameModal(),
        };

        this.init_translator().finally(() => {
            warnNoMobile();
            this.state.loading = false;
            this.refresh();
        });
    }

    /**
     * Initialize the translation library
     * @returns {Boolean}
     */
    async init_translator() {
        await translator.init({
            translations_url: app_translations_url,
            supported_languages: ["en", "fr", "es"],
        }).catch(err => {
            console.error(err);
            (new NotifPopup({ error: true, message: "Error: page cannot be translated" })).pop();
        });

        return true;
    }

    /**
     * Opens the game publishing/update/delete dialog window.
     */
    open_publish_modal() {
        this.components.publish_game_modal.open()
    }

    /**
     * Closes the game game publishing/update/delete dialog window
     */
    close_publish_modal() {
        this.components.publish_game_modal.close()
    }

    /**
     * Refreshes the rendering from page root
     */
    refresh() {
        obj2htm.subRender(this.render(), document.getElementById("games-page"), { mode: "replace" })
    }

    /**
     * @returns {Object} object representation of the html to render for this component
     */
    render() {
        const { loading } = this.state;
        return {
            tag: "main",
            id: "games-page",
            contents: [
                loading && new LoaderModal().render(),
                {
                    tag: "header", class: "page-header",
                    contents: [
                        {
                            tag: "a", href: "/", contents: [
                                { tag: "img", src: `${images_url}logo_mentalo_text.png`, class: "mtlo-logo-txt" },
                            ]
                        },
                        { tag: "h1", contents: t("Games"), class: "page-title" }
                    ]
                },
                this.components.game_list.render(),
                this.menu_panel.render()
            ],
        }
    }
}

module.exports = GamesPage;