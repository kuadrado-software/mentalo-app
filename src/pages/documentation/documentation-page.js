"use strict";

const translator = require("ks-cheap-translator");
const t = translator.trad.bind(translator);
const { docs_translations_url, images_url } = require("../../constants");
const LoaderModal = require("../../loader-modal");
const MenuPanel = require("../../menu-panel/menu-panel");
const TooltipsManager = require("tooltips-manager");
const { chapters } = require("./book/book");
const NotifPopup = require("../../notif-popup");

/**
 * The main component of the document page
 */
class DocumentationPage {
    constructor() {

        this.menu_panel = new MenuPanel({
            on_changed_language: this.refresh.bind(this, true),
        });

        this.tooltips_manager = new TooltipsManager();

        this.state = {
            loading: true,
            display_book: {
                section_index: -1,
                chapter: chapters[0]
            },
        };

        this.init();
    }

    /**
     * Initialize the text translator and the listeners for virtual page navigation.
     */
    init() {
        this.on_pop_history_state = this.on_pop_history_state.bind(this);

        window.addEventListener("popstate", this.on_pop_history_state);

        this.init_translator().finally(() => {
            this.handle_url_params();

            this.state.loading = false;
            this.refresh();
        });

    }

    /**
     * Initialize the text translation library
     */
    async init_translator() {
        await translator.init({
            translations_url: docs_translations_url,
            supported_languages: ["en", "fr", "es"],
        }).catch(e => {
            console.error(e);
            (new NotifPopup({ error: true, message: "Error: page cannot be translated" })).pop();
        });
    }

    /**
     * The callback called when the browser navigation buttons are clicked
     */
    on_pop_history_state() {
        this.handle_url_params();
        this.refresh();
    }

    /**
     * Update the state of the book (chapter & section) accordingly the the uri querystring.
     */
    handle_url_params() {
        const url_params = (new URL(document.location)).searchParams;

        let find_param_chap, find_param_section;

        const chapter_slug_param = (function () {
            // validate the parameter before using
            const c_slug = url_params.get("chapter");
            find_param_chap = chapters.find(c => c.slug === c_slug);
            return find_param_chap ? c_slug : undefined;
        })();

        const section_slug_param = (function () {
            const sec_slug = url_params.get("section");
            if (!chapter_slug_param) return undefined;
            find_param_section = find_param_chap.sections.find(s => s.slug === sec_slug);
            return find_param_section ? sec_slug : undefined;
        })();

        const chapter = find_param_chap || chapters[0];
        const section_index = section_slug_param !== undefined ? find_param_chap.sections.indexOf(find_param_section) : -1;

        // If a correction was made up there, update the url
        const replace_url = new URL(document.location);
        const params = replace_url.searchParams;
        let updated_url = false;

        if (chapter.slug !== chapter_slug_param) {
            params.set("chapter", chapter.slug);
            updated_url = true;
        }

        if (section_index === -1 && params.has("section")) {
            params.delete("section");
            updated_url = true;
        }

        if (updated_url) {
            replace_url.search = params.toString();
            history.replaceState({}, "", replace_url.toString());
        }

        this.state.display_book.chapter = chapter;
        this.state.display_book.section_index = section_index;
    }

    /**
     * Handles the navigation to a different book section
     * @param {Chapter} chapter 
     * @param {Integer} section_index 
     * @returns 
     */
    handle_navigate_to_book_section(chapter, section_index) {
        if (chapter === this.state.display_book.chapter && section_index === this.state.display_book.section_index) return;

        this.state.display_book.chapter = chapter;
        this.state.display_book.section_index = section_index;

        const current_url = new URL(document.location);
        const search_params = current_url.searchParams;

        search_params.set("chapter", chapter.slug);

        if (section_index !== -1) {
            search_params.set("section", chapter.sections[section_index].slug);
        } else if (search_params.has("section")) {
            search_params.delete("section")
        }

        current_url.search = search_params.toString();
        const updated_url = current_url.toString();
        history.pushState({}, "", updated_url);
        this.refresh();
        window.scrollTo(0, 0);
    }

    /**
     * Handle the clicks on the pagination arrows
     * @param {String} direction "next" or "prev"
     */
    handle_navigate_to_page(direction) {
        const { chapter, section_index } = this.state.display_book;
        const current_chap_index = chapters.indexOf(chapter);
        let change_chapter = false;
        switch (direction) {
            case "next":
                change_chapter = section_index === chapter.sections.length - 1;
                if (change_chapter) {
                    // if chapter is already the last chapter, button is not rendered so the fonction will never be called is this case.
                    this.handle_navigate_to_book_section(
                        chapters[current_chap_index + 1],
                        -1
                    );
                } else {
                    this.handle_navigate_to_book_section(chapter, section_index + 1)
                }
                break;
            case "prev":
                // if chapter is already the first one, the calling button is not rendered.
                change_chapter = section_index === -1;

                if (change_chapter) {
                    const prev_chap = chapters[current_chap_index - 1]
                    this.handle_navigate_to_book_section(prev_chap, prev_chap.sections.length - 1)
                } else {
                    this.handle_navigate_to_book_section(chapter, section_index - 1);
                }
                break;
        }
    }

    /**
     * Refreshes the rendering from this component
     * @param {Boolean} refresh_menu Wether the menu panel must be refreshed or not
     */
    refresh(refresh_menu = false) {
        refresh_menu && this.menu_panel.refresh();
        obj2htm.subRender(this.render(), document.getElementById("mtlo-documentation-page"), { mode: "replace" });
    }

    /**
     * @returns {Object} The object representation of the html to render for the pagination arrow buttons
     */
    render_page_navigation() {
        const { chapter, section_index } = this.state.display_book;

        const display_go_prev = chapters.indexOf(chapter) > 0 || section_index > -1;

        const display_go_next = chapters.indexOf(chapter) + 1 <= chapters.length - 1
            || section_index + 1 <= chapter.sections.length - 1;

        return {
            tag: "div",
            class: "pages-navigation",
            contents: [
                display_go_prev ? {
                    tag: "button", contents: "<",
                    tooltip: t("Previous page"),
                    onclick: this.handle_navigate_to_page.bind(this, "prev")
                } : { tag: "span" },
                display_go_next ? {
                    tag: "button", contents: ">",
                    tooltip: t("Next page"),
                    onclick: this.handle_navigate_to_page.bind(this, "next")
                } : { tag: "span" }
            ]
        }
    }

    /**
     * @returns {Object} the object representation of the html to render for the currently selected page.
     */
    render_page() {
        const on_summary_item_click = this.handle_navigate_to_book_section.bind(this);
        const { section_index, chapter } = this.state.display_book;
        return section_index !== -1 ?
            chapter.sections[section_index].render_page()
            : chapter.render_page(on_summary_item_click);
    }

    /**
     * @returns {Object} The object representation of the html to render for this component when it's not in loading state.
     */
    render_book_view() {
        return {
            tag: "div",
            id: "mtlo-documentation-book-view",
            contents: [
                {
                    tag: "header", class: "page-header",
                    contents: [
                        {
                            tag: "a", href: "/", contents: [
                                { tag: "img", src: `${images_url}logo_mentalo_text.png`, class: "mtlo-logo-txt" },
                            ]
                        },
                        { tag: "h1", contents: t("Documentation"), class: "page-title" }
                    ]
                },
                {
                    tag: "aside",
                    contents: [
                        {
                            tag: "nav",
                            on_render: node => {
                                // Adjust chapters menu scroll distance for mobile display
                                const active_lines = node.querySelectorAll(".active")
                                const lower_active_line = active_lines.item(active_lines.length - 1);
                                node.scrollTo(0, lower_active_line.offsetTop);
                            },
                            contents: [
                                {
                                    tag: "ul", contents: chapters.map(chap => {
                                        const chap_active = chap === this.state.display_book.chapter;
                                        return {
                                            tag: "li",
                                            contents: [
                                                {
                                                    tag: "h4", contents: t(chap.title),
                                                    class: "book-chap-title"
                                                        + (chap_active ? " active" : "")
                                                        + (chap.sections.length > 0 ? " with-sections" : ""),
                                                    onclick: this.handle_navigate_to_book_section.bind(this, chap, -1)
                                                },
                                                chap_active && chap.sections.length > 0 && {
                                                    tag: "ul",
                                                    class: "book-chap-sections-list",
                                                    contents: chap.sections.map((sec, i) => {
                                                        const section_active = i === this.state.display_book.section_index;
                                                        return {
                                                            tag: "li",
                                                            class: "book-section-index" + (section_active ? " active" : ""),
                                                            contents: t(sec.title),
                                                            onclick: this.handle_navigate_to_book_section.bind(this, chap, i)
                                                        }
                                                    })
                                                }
                                            ],
                                        }
                                    })
                                }
                            ]
                        }
                    ]
                },
                {
                    tag: "section",
                    contents: [
                        this.render_page(),
                        this.render_page_navigation(),
                    ],
                },
                this.menu_panel.render(),
            ]
        }
    }

    /**
     * @returns {Object} the object representation of the html main element containg the book view
     */
    render() {
        return {
            tag: "main",
            id: "mtlo-documentation-page",
            contents: this.state.loading ? [new LoaderModal().render()] : [this.render_book_view()]
        }
    }
}

module.exports = DocumentationPage;