module.exports = {
    tag: "svg",
    xmlns: "http://www.w3.org/2000/svg",
    width: "36.654827mm",
    height: "42.364216mm",
    viewBox: "0 0 36.654827 42.364216",
    version: "1.1",
    class: "svg-icon icon-play",
    contents: [
        {
            tag: "g", transform: "translate(-42.145857,-97.502415)", contents: [
                {
                    tag: "path",
                    d: "M 42.145857,139.86663 V 97.502415 l 36.654828,21.139775 v 0.0423 0.0423 z",
                }
            ]
        },
    ]
};