"use strict";

const translator = require("ks-cheap-translator");
const t = translator.trad.bind(translator);

/**
 * The component that displayed the general settings form in the side toolbar
 */
class GeneralSettingsForm {
    constructor(params) {
        this.params = params;
    }

    /**
     * Handles an input change for any property
     * @param {String} property 
     * @param {Event} e 
     */
    handle_change_property(property, e) {
        this.params.project.game_ui_options.general[property] = e.target.value;
        this.params.project_view.refresh();
    }

    /**
     * Handle select input change for image ratio (4:3, 16:9, etc)
     * @param {Event} e 
     */
    handle_change_image_ratio(e) {
        this.params.project.game_ui_options.general.animation_canvas_dimensions.ratio = e.target.value;
        this.params.project_view.refresh();
    }

    /**
     * @returns {Object} the object representation of the html to render for this component
     */
    render() {
        const current_settings = this.params.project.game_ui_options.general;
        return {
            tag: "ul", class: "ui-settings-form",
            contents: [
                {
                    tag: "li", contents: [
                        {
                            tag: "div", class: "settings-title", contents: [
                                { tag: "h4", contents: t("General settings") }
                            ]
                        },
                        {
                            tag: "ul", class: "settings-list", contents: [
                                {
                                    tag: "div", class: "setting-block", contents: [
                                        { tag: "label", contents: t("Background color") },
                                        {
                                            tag: "input", type: "color", value: current_settings.background_color,
                                            onchange: this.handle_change_property.bind(this, "background_color")
                                        }
                                    ]
                                },
                                {
                                    tag: "div", class: "setting-block", contents: [
                                        { tag: "label", contents: t("Image ratio") },
                                        {
                                            tag: "select", contents: ["4:3", "16:9", "21:9", "14:10", "19:10", "1:1"].map(ratio => {
                                                return {
                                                    tag: "option", value: ratio, contents: ratio,
                                                    selected: current_settings.animation_canvas_dimensions.ratio === ratio,
                                                    onclick: e => e.stopImmediatePropagation(),
                                                    //              ^ Fixes a minor bug where the button hidden behind the select dropdown gets also clicked
                                                }
                                            }),
                                            onchange: this.handle_change_image_ratio.bind(this)
                                        }
                                    ]
                                },
                            ]
                        }

                    ]
                },
            ]
        }
    }
}

module.exports = GeneralSettingsForm;