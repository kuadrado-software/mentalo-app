"use strict";

const { FrameRateController, font_tools, shape_tools } = require("mentalo-engine");
const { get_canvas_font, get_canvas_char_size } = font_tools;
const { ui_config } = require("../../../constants");

/**
 * The component to render the image of the scene on a 2D canvas
 */
class SceneImageCanvas {
    /**
     * @param {Object} params Required params are project<Project> and ui_options<Object>
     */
    constructor(params) {
        this.params = params;

        this.scene = this.params.project.get_scene();
        this.scene.animation.reset_frame();
        this.id = "scene-image-canvas";
        this.fps_controller = new FrameRateController(ui_config.project_view.animation_canvas_frame_rate);
        this.watch_and_start();
        this.framecount = 0;
        this.ui_state = {
            cursor_style: [],
            dragging_object: false,
        };

        window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
    }

    /**
     * Attaches a callback to start the displaying of the scene (animation, objects and text box)
     * as soon as the canvas has been rendered to the DOM
     */
    watch_and_start() {
        this.canvas_ready_watcher = () => {
            if (document.getElementById(this.id)) {
                this.start();
            }
        };

        window.addEventListener(obj2htm.event_name, this.canvas_ready_watcher);
    }

    /**
     * Clear the listeners for the rendering of the canvas in the DOM and starts to run the scene image rendering.
     */
    start() {
        window.removeEventListener(obj2htm.event_name, this.canvas_ready_watcher);
        this.init_canvas();
        this.run_scene();
    }

    /**
     * Initialize the canvas element and calculates all the necessary measures for the canvas itself, the text box, etc.
     * Also initialize all the event listeners used to interact with the scene elements (objects, closing icons, etc)
     */
    init_canvas() {
        const { ui_options } = this.params;
        const { game_ui_options } = this.params.project;

        this.canvas = document.getElementById(this.id);
        this.ui_state.cursor_style.current = this.canvas.style.cursor;

        this.ctx = this.canvas.getContext("2d");

        this.ctx.mozImageSmoothingEnabled = false;
        this.ctx.webkitImageSmoothingEnabled = false;
        this.ctx.msImageSmoothingEnabled = false;
        this.ctx.imageSmoothingEnabled = false;

        // Precalculations for textbox
        this.ctx.textBaseline = "top";
        const char_size = get_canvas_char_size(this.ctx, game_ui_options.text_boxes);
        const line_height = char_size.text_line_height;
        const text_box_padding = game_ui_options.text_boxes.padding;
        const text_box_margin = game_ui_options.text_boxes.margin;
        const error_offset = 3 * char_size.width;

        const chars_per_line =
            (this.canvas.width - text_box_padding - text_box_margin - error_offset)
            / char_size.width;

        const text = this.scene.text_box;

        const output_lines = [];
        const lines = text.split("\n");

        lines.forEach(line => {
            const res = [""];
            let target_i = 0;
            for (const word of line.split(" ")) {
                if ((res[target_i] + word + " ").length > chars_per_line) {
                    res.push("");
                    target_i++;
                }
                res[target_i] += word + " ";
            }
            res.forEach(l => output_lines.push(l))
        });

        const box_height = (output_lines.length * line_height) + (text_box_padding * 2) - char_size.interline_height;

        const box_position = {
            x: text_box_margin,
            y: this.canvas.height - box_height - text_box_margin,
        };

        const box_width = this.canvas.width - (text_box_margin * 2);

        this.text_box = {
            lines: output_lines,
            line_height,
            box_height,
            box_width,
            margin: text_box_margin,
            padding: text_box_padding,
            position: box_position,
        };

        const icon_size = 30;
        const center = {
            x: box_position.x + box_width - 5,
            y: box_position.y + 5
        };

        const position = {
            x: center.x - icon_size / 2,
            y: center.y - icon_size / 2
        };

        const cross_coords = {
            top: position.y + 10,
            left: position.x + 10,
            right: position.x + icon_size - 10,
            bottom: position.y + icon_size - 10
        };

        this.text_box.closing_icon = {
            sq_size: icon_size,
            center,
            position,
            cross_coords
        };

        this.close_mouseover_listener = e => {
            if (e.offsetX > cross_coords.left - 10
                && e.offsetX < cross_coords.right + 10
                && e.offsetY > cross_coords.top - 10
                && e.offsetY < cross_coords.bottom + 10) {
                this.ui_state.cursor_style.push({ obj_name: "close icon", style: "pointer" });
            } else {
                const found_style = this.ui_state.cursor_style.find(s => s.obj_name === "close icon");
                found_style && this.ui_state.cursor_style.splice(this.ui_state.cursor_style.indexOf(found_style), 1);
            }
        };

        this.close_text_box_listener = e => {
            if (e.offsetX > cross_coords.left - 10
                && e.offsetX < cross_coords.right + 10
                && e.offsetY > cross_coords.top - 10
                && e.offsetY < cross_coords.bottom + 10) {
                this.canvas.removeEventListener("click", this.close_text_box_listener);
                this.canvas.removeEventListener("mousemove", this.close_mouseover_listener);
                this.canvas.removeEventListener("dblclick", this.text_box_dblclick_listener);
                ui_options.on_close_text_box();
            }
        };

        this.text_box_dblclick_listener = e => {
            const bounds = {
                top: this.text_box.position.y,
                right: this.text_box.position.x + this.text_box.box_width,
                bottom: this.text_box.position.y + this.text_box.box_height,
                left: this.text_box.position.x,
            };

            if (e.offsetX > bounds.left - 10
                && e.offsetX < bounds.right + 10
                && e.offsetY > bounds.top - 10
                && e.offsetY < bounds.bottom + 10) {
                ui_options.on_text_box_dblclick();
            }
        };

        if (ui_options.show_text_box) {
            this.canvas.addEventListener("click", this.close_text_box_listener);
            this.canvas.addEventListener("mousemove", this.close_mouseover_listener);
            this.canvas.addEventListener("dblclick", this.text_box_dblclick_listener);
        }

        // Prepare event listeners for game objects
        this.scene.game_objects.forEach(gob => {
            gob.clear_listeners(this.canvas);

            const attach_listeners = () => {

                // Handle drag n drop (edit object position) listener
                gob.ui_state.drag_drop_listeners.mousedown = edown => {
                    const { top, right, bottom, left } = gob.get_bounds();

                    if (edown.offsetX > left
                        && edown.offsetX < right
                        && edown.offsetY > top
                        && edown.offsetY < bottom) {

                        edown.stopImmediatePropagation();

                        // Put the clicked element on top of rendering
                        const clicked_i = this.scene.game_objects.indexOf(gob);
                        const last_i = this.scene.game_objects.length - 1;
                        const save_last = this.scene.game_objects[last_i];
                        this.scene.game_objects[last_i] = this.scene.game_objects[clicked_i];
                        this.scene.game_objects[clicked_i] = save_last;

                        const cur_pos = gob.position;

                        const pos_offset = {
                            x: edown.offsetX - cur_pos.x,
                            y: edown.offsetY - cur_pos.y
                        };

                        this.ui_state.dragging_object = true;

                        gob.ui_state.drag_drop_listeners.mousemove = edrag => {
                            const new_pos = {
                                x: edrag.offsetX - pos_offset.x,
                                y: edrag.offsetY - pos_offset.y,
                            };
                            gob.set_position(new_pos);
                        };

                        gob.ui_state.drag_drop_listeners.mouseup = eup => {
                            const final_pos = {
                                x: eup.offsetX - pos_offset.x,
                                y: eup.offsetY - pos_offset.y,
                            };

                            gob.set_position(final_pos);
                            gob.clear_drop(this.canvas);
                        };

                        this.canvas.addEventListener("mousemove", gob.ui_state.drag_drop_listeners.mousemove);
                        this.canvas.addEventListener("mouseup", gob.ui_state.drag_drop_listeners.mouseup);

                    } else {
                        gob.clear_drop(this.canvas);
                    }
                };

                this.canvas.addEventListener("mousedown", gob.ui_state.drag_drop_listeners.mousedown);

                // Handle cursor state change when mouse over an object
                gob.ui_state.cursor_listeners.mousemove = e => {
                    const { top, right, bottom, left } = gob.get_bounds();

                    if (e.offsetX > left
                        && e.offsetX < right
                        && e.offsetY > top
                        && e.offsetY < bottom) {
                        this.ui_state.cursor_style.push({ obj_name: gob.name, style: "move" });
                    } else {
                        const found_style = this.ui_state.cursor_style.find(s => s.obj_name === gob.name);
                        found_style && this.ui_state.cursor_style.splice(this.ui_state.cursor_style.indexOf(found_style));
                    }
                };

                this.canvas.addEventListener("mousemove", gob.ui_state.cursor_listeners.mousemove);

                // Handle object double click listener (edit object)
                gob.ui_state.edit_listeners.dblclick = e => {
                    const { top, right, bottom, left } = gob.get_bounds();

                    if (e.offsetX > left
                        && e.offsetX < right
                        && e.offsetY > top
                        && e.offsetY < bottom) {
                        ui_options.on_game_object_dblclick(gob);
                    }
                };

                this.canvas.addEventListener("dblclick", gob.ui_state.edit_listeners.dblclick);
            };

            if (!gob.image.complete) {
                gob.image.onload = () => {
                    attach_listeners();
                };
            } else {
                attach_listeners();
            }
        });
    }

    /**
     * Draw the scene animation on the 2D context of the canvas.
     */
    draw_animation() {
        const anim = this.scene.animation;
        anim.update_frame(this.framecount);
        const dim = anim.dimensions;
        const w = dim.width;
        const h = dim.height;
        const offsetX = anim.frame * w;

        const cw = this.canvas.width;
        const ch = this.canvas.height;

        // center the image
        if (!anim.canvas_precalc) {
            anim.canvas_precalc = {
                dx: 0, dy: 0, dw: 0, dh: 0,
            };
            if (w / h > cw / ch) {
                // image is more panoramic than canvas
                anim.canvas_precalc.dw = cw;
                anim.canvas_precalc.dh = cw * (h / w);
                // center vertically
                anim.canvas_precalc.dy = (ch - anim.canvas_precalc.dh) / 2;
            } else if (cw / ch > w / h) {
                // canvas is more panoramic
                anim.canvas_precalc.dh = ch;
                anim.canvas_precalc.dw = ch * (w / h);
                // center horizontally
                anim.canvas_precalc.dx = (cw - anim.canvas_precalc.dw) / 2;
            } else {
                // identical ratio
                anim.canvas_precalc.dh = ch;
                anim.canvas_precalc.dw = cw;
            }
        }

        const { dx, dy, dw, dh } = anim.canvas_precalc;
        this.ctx.drawImage(
            anim.image,
            offsetX, 0,
            w, h,
            dx, dy,
            dw, dh
        );
    }

    /**
     * Draw the images of the objects at their position on the scene image on the 2D context of the canvas.
     */
    draw_game_objects() {
        this.scene.game_objects.filter(o => o.image.complete).forEach(gob => {
            const pos = gob.position;
            this.ctx.drawImage(gob.image, pos.x, pos.y);
        });
    }

    /**
     * Draw the scene text box on the canvas 2D context.
     */
    draw_text_box() {
        const { game_ui_options } = this.params.project;
        const { box_height, box_width, lines, padding, line_height, position, closing_icon } = this.text_box;

        this.ctx.font = get_canvas_font(game_ui_options.text_boxes);
        this.ctx.textAlign = game_ui_options.text_boxes.text_align;

        const box_pos = position;

        shape_tools.draw_rect(this.ctx, box_pos.x, box_pos.y, box_width, box_height, {
            fill_color: game_ui_options.text_boxes.background_color,
            rounded_corners_radius: game_ui_options.text_boxes.rounded_corners_radius,
            border: {
                width: game_ui_options.text_boxes.border_width,
                color: game_ui_options.text_boxes.font_color,
            },
        });

        this.ctx.fillStyle = game_ui_options.text_boxes.font_color;

        const get_text_position = () => {
            switch (this.ctx.textAlign) {
                case "left":
                    return {
                        x: box_pos.x + padding,
                        y: box_pos.y + padding,
                    }
                case "right":
                    return {
                        x: box_pos.x + box_width - padding,
                        y: box_pos.y + padding,
                    }
                case "center":
                    return {
                        x: box_pos.x + box_width / 2,
                        y: box_pos.y + padding,
                    }
                default:
                    return {
                        x: box_pos.x + padding,
                        y: box_pos.y + padding,
                    }
            }
        };

        const text_pos = get_text_position();

        lines.forEach(line => {
            this.ctx.fillText(line, text_pos.x, text_pos.y);
            text_pos.y += line_height;
        });

        // draw closing cross icon
        const { sq_size, center, cross_coords } = closing_icon;

        const draw_arc = () => this.ctx.arc(center.x, center.y, sq_size / 2, 0, 2 * Math.PI);

        this.ctx.fillStyle = game_ui_options.text_boxes.background_color;
        this.ctx.beginPath();
        draw_arc();
        this.ctx.fill();

        this.ctx.strokeStyle = game_ui_options.text_boxes.font_color;
        this.ctx.lineWidth = 2;
        this.ctx.beginPath();
        draw_arc();
        this.ctx.stroke();

        this.ctx.beginPath();
        this.ctx.moveTo(cross_coords.left, cross_coords.top);
        this.ctx.lineTo(cross_coords.right, cross_coords.bottom);
        this.ctx.moveTo(cross_coords.left, cross_coords.bottom)
        this.ctx.lineTo(cross_coords.right, cross_coords.top);
        this.ctx.stroke();
    }

    /**
     * Increments the framecount state for the displaying of the scene animation frame sequence.
     */
    update_framecount() {
        this.framecount = this.framecount + 1 <= Number.MAX_SAFE_INTEGER ? this.framecount + 1 : 0;
    }

    /**
     * Sets the style of the mouse cursor accordingly to what is being hovered.
     */
    update_cursor_style() {
        if (this.ui_state.cursor_style.length > 0) {
            this.canvas.style.cursor = this.ui_state.cursor_style[0].style;
        } else {
            this.canvas.style.cursor = "unset";
        }
    }

    /**
     * Calls all the methods to draw the different scene elements on the canvas 2D context.
     * Calls window.requestAnimationFrame so this is called repetitively.
     */
    render_scene() {
        const { ui_options } = this.params;
        if (this.fps_controller.nextFrameReady()) {
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.scene.animation.initialized && this.draw_animation();
            if (this.scene.game_objects.length > 0) {
                this.draw_game_objects();
            }
            if (this.scene.text_box && ui_options.show_text_box) {
                this.draw_text_box();
            }
            this.update_framecount();
        }

        this.update_cursor_style();

        window.scene_animation_request_id = requestAnimationFrame(this.render_scene.bind(this))
    }

    /**
     * Triggers the rendering of the scene.
     * Initialize the requestAnimationFrame callback 
     */
    run_scene() {
        const anim = this.scene.animation;

        if (window.scene_animation_request_id) {
            window.cancelAnimationFrame(window.scene_animation_request_id);
        }

        if (!anim.image.complete) {
            anim.image.onload = () => {
                anim.update_dimensions();
            };
        } else {
            anim.update_dimensions()
        }

        window.scene_animation_request_id = requestAnimationFrame(this.render_scene.bind(this))
    }

    /**
     * @returns {Object} The object representation of the html element to render for this component
     */
    render() {
        const style = this.params.project.game_ui_options.general;
        return {
            tag: "canvas",
            width: style.animation_canvas_dimensions.width,
            height: style.animation_canvas_dimensions.height(),
            id: this.id,
        }
    }
}

module.exports = SceneImageCanvas