"use strict";
const translator = require("ks-cheap-translator");
const icon_trash_recycle = require("../../../../../jsvg/icon_trash_recycle");
const t = translator.trad.bind(translator);

class ChooseExistingImageInput {
    constructor(params) {
        this.params = params;
        this.images = this.params.get_images();

        this.set_selected_image();

        this.state = {
            list_scroll: 0,
            init_scroll: false,
        };
    }

    set_selected_image() {
        let found_selected = false;
        for (const img of this.images) {
            const selected = this.params.edit_scene.animation.name === img.name
            img.selected = selected;
            if (selected) {
                found_selected = true;
            }
        }

        if (!found_selected && this.images.length > 0) {
            this.images[0].selected = true;
        }
    }

    refresh_images() {
        this.images = this.params.get_images();
    }

    refresh_list() {
        obj2htm.subRender(
            this.render_list_images(),
            document.querySelector("#choose-existing-image-input ul"),
            { mode: "replace" }
        );
    }

    handle_fix_list_scroll_top(list_node) {
        if (!this.state.init_scroll) {
            const selected_li = list_node.querySelector(".selected");
            list_node.scrollTo(0, selected_li.offsetTop);
            this.state.list_scroll = list_node.scrollTop;
            this.state.init_scroll = true;
        } else {
            list_node.scrollTo(0, this.state.list_scroll);
        }
    }

    handle_select_image(img) {
        let value = "";
        this.images.forEach(im_res => {
            im_res.selected = im_res === img;
            if (im_res.selected) {
                value = im_res.name;
            }
        });

        document.getElementById("choose-existing-image-input-value").value = value;

        this.refresh_list();
    }

    handle_scroll_list(e) {
        this.state.list_scroll = e.target.scrollTop;
    }

    handle_delete_unused_images() {
        this.params.delete_unused_images();
        this.refresh_images();
        this.refresh_list();
    }

    render_list_images() {
        return {
            tag: "ul",
            onscroll: this.handle_scroll_list.bind(this),
            on_render: this.handle_fix_list_scroll_top.bind(this),
            contents: this.images.map(img_res => {
                return {
                    tag: "li",
                    class: img_res.selected ? "selected" : "",
                    style_rules: {
                        border: img_res.selected ? "2px solid rgb(40, 199, 61)" : "2px solid transparent",
                        opacity: img_res.selected ? 1 : .6,
                    },
                    contents: [
                        {
                            tag: "img",
                            src: img_res.src,
                            tooltip: img_res.name + (img_res.frame_nb && img_res.frame_nb > 1 ? ` (${img_res.frame_nb}) ${t("frames")})` : ""),
                        },
                    ],
                    onclick: this.handle_select_image.bind(this, img_res)
                }
            })
        }
    }

    render() {
        return {
            tag: "div",
            id: "choose-existing-image-input",
            contents: [
                {
                    tag: "input",
                    type: "hidden",
                    id: "choose-existing-image-input-value",
                    value: this.images.find(im => im.selected).name
                },
                this.render_list_images(),
                {
                    tag: "button",
                    class: "action-button",
                    tooltip: t("Delete unused images"),
                    contents: [{ ...icon_trash_recycle }],
                    onclick: this.handle_delete_unused_images.bind(this)
                }
            ]
        }
    }
}

module.exports = ChooseExistingImageInput;