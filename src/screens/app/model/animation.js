"use strict";

const { MtlAnimation } = require("mentalo-engine");

/**
 * The type used for the animation field of a Scene
 */
class Animation extends MtlAnimation {

    /**
     * Append some changes to the instance, replaces all modified fields
     * @param {Object} data 
     */
    update_data(data) {
        super.init(data);

        Object.entries(data).forEach((entry) => {
            const [key, value] = entry;
            this[key] = value;
        });

        this.reset_frame();
    }

    /**
     * @returns {JSON} The instance formatted as a litteral object and serialized
     */
    serialized() {
        return JSON.stringify(this.as_litteral());
    }

    /**
     * @returns {Object} The instance formatted as a litteral object to be safely serialized
     */
    as_litteral() {
        return {
            name: this.name,
            speed: this.speed,
            play_once: this.play_once,
        }
    }
}

module.exports = Animation;