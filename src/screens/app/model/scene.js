"use strict";

const GameObject = require("./game_object");
const Choice = require("./choice");
const SoundTrack = require("./sound-track");
const { MtlScene, SCENE_TYPES } = require("mentalo-engine");
const Animation = require("./animation");
const translator = require("ks-cheap-translator");
const t = translator.trad.bind(translator);

/**
 * The type used for the scenes field of a Project instance
 */
class Scene extends MtlScene {
    constructor(shared_state) {
        super(shared_state);
        this.name = `${t("Scene")} 1`;
        this.animation = new Animation(this.shared_state);
        this.sound_track = new SoundTrack(this.shared_state);
    }

    /**
     * Sets the type of the scene
     * @param {String} type Can be eiher "Playable" or "Cinematic"
     */
    set_type(type) {
        if (type === SCENE_TYPES.CINEMATIC) {
            this.game_objects = [];
            this.choices = [];
            this.text_box = "";
        } else {
            this.cinematic_duration = 0;
        }
        this._type = type;
    }

    /**
     * Adds a new Choice instance to the choices field of this Scene instance
     * @param {Choice} choice 
     */
    add_choice(choice) {
        choice.text && this.choices.push(choice);
    }

    /**
     * Replaces a choice by another one
     * @param {Choice} choice 
     * @param {Choice} updated_choice 
     */
    update_choice(choice, updated_choice) {
        this.choices.splice(this.choices.indexOf(choice), 1, updated_choice);
    }

    /**
     * Removes a choice from this Scene instance
     * @param {Choice} choice 
     */
    remove_choice(choice) {
        this.choices.splice(this.choices.indexOf(choice), 1);
    }

    /**
     * Adds a new GameObject instance to the game_objects field of this Scene instance
     * @param {GameObject} obj
     */
    add_game_object(obj) {
        this.game_objects.push(obj);
    }

    /**
     * Remove a GameObject from the game_objects field of this instance.
     * @param {GameObject} gobj 
     */
    remove_game_object(gobj) {
        const find_gobj = this.game_objects.find(o => o === gobj);
        this.game_objects.splice(this.game_objects.indexOf(find_gobj), 1);
    }

    /**
     * Populates the instance from data of a scene in its litteral version
     * @param {Object} data 
     */
    load_data(data) {
        super.load_data(data);

        const animation = new Animation(this.shared_state);
        animation.init(data.animation);
        this.animation = animation;

        const sound_track = new SoundTrack(this.shared_state);
        sound_track.init(data.sound_track);
        this.sound_track = sound_track;

        this.choices = data.choices.map(c => new Choice(c));

        this.game_objects = data.game_objects.map(gob => {
            const inst = new GameObject(this.shared_state);
            inst.load_data(gob);
            return inst;
        });

    }

    /**
     * @returns {JSON} The instance serialized
     */
    serialized() {
        return JSON.stringify(this.as_litteral());
    }

    /**
     * @returns {Object} This instance formatted as a litteral object
     */
    as_litteral() {
        return {
            name: this.name,
            _type: this._type,
            animation: this.animation.as_litteral(),
            sound_track: this.sound_track.as_litteral(),
            choices: this.choices.map(c => c.as_litteral()),
            text_box: this.text_box,
            cinematic_duration: this.cinematic_duration,
            game_objects: this.game_objects.map(gob => gob.as_litteral()),
            end_cinematic_options: this.end_cinematic_options,
        };
    }
}

module.exports = Scene;