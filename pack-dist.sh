#!/bin/bash
tar -cvzf ./dist/mentalo-app-dist.tar.gz --exclude 'dist' \
    --exclude '.git' \
    --exclude 'node_modules' \
    --exclude 'docker-compose.yml' \
    --exclude 'Dockerfile' \
    --exclude 'public/*.js' \
    --exclude 'public/*.css' \
    --exclude 'public/mockup-data' \
    --exclude '.vscode' \
    --exclude '.dockerignore' \
    --exclude '.gitignore' \
    --exclude 'Makefile' \
    --exclude 'pack-dist.sh' \
    --exclude 'public/*.css.map' ./
